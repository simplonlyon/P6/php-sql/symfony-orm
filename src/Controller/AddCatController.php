<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Form\KittenType;
use App\Entity\Kitten;
use App\Entity\Squat;

class AddCatController extends Controller
{
    /**
     * @Route("/add/cat", name="add_cat")
     */
    public function index(Request $request)
    {
        $kitten = new Kitten();
        $kitten->addSquat(new Squat());

        $form = $this->createForm(KittenType::class, $kitten);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /**
             * Pour mettre sur la bdd les informations d'un formulaire
             * on fait le formulaire comme d'hab, mais ici, on utilise
             * le manager et le persist/flush
             */

            $em = $this->getDoctrine()->getManager();
            
            $em->persist($form->getData());

            $em->flush();

        }

        return $this->render('add_cat/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
