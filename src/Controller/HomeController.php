<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Kitten;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        // $kitty = new Kitten();
        // $kitty->setName("Moushtache");
        // $kitty->setBreed("Persan");
        // $kitty->setBirthdate(new \DateTime("2018-06-01"));

        // $em = $this->getDoctrine()->getManager();
        
        // $em->persist($kitty);

        // $em->flush();
        // dump($kitty);
        
        //Pour les méthodes de lecture de la bdd, on utilisera plutôt
        //le repository à la place du manager.
        $repo = $this->getDoctrine()->getRepository(Kitten::class);
        //findAll pour trouver toutes les entrées d'une table
        dump($repo->findAll());
        //find pour trouver une instance spécifique par son id
        dump($repo->find(1));
        //findBy pour trouver une liste d'instance, selon
        // un ou plusieurs critères sous forme de tableau associatif
        dump($repo->findBy(['breed' => 'Persan']));

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
