<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KittenRepository")
 */
class Kitten
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $breed;

    /**
     * @ORM\Column(type="date")
     */
    private $birthdate;

    //Le cascade={"persist"} en bout de ligne sert à dire que si jamais
    //on essaye de faire persister un Kitten qui contient des Squats
    //qui n'existent pas encore en bdd, alors ces squats seront persistés également
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Squat", mappedBy="cats", cascade={"persist"})
     */
    private $squats;

    public function __construct()
    {
        $this->squats = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreed(): ?string
    {
        return $this->breed;
    }

    public function setBreed(string $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return Collection|Squat[]
     */
    public function getSquats(): Collection
    {
        return $this->squats;
    }

    public function addSquat(Squat $squat): self
    {
        if (!$this->squats->contains($squat)) {
            $this->squats[] = $squat;
            $squat->addCat($this);
        }

        return $this;
    }

    public function removeSquat(Squat $squat): self
    {
        if ($this->squats->contains($squat)) {
            $this->squats->removeElement($squat);
            $squat->removeCat($this);
        }

        return $this;
    }
}
