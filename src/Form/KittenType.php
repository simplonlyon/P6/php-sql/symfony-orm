<?php

namespace App\Form;

use App\Entity\Kitten;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class KittenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('breed')
            ->add('birthdate', BirthdayType::class)
            /**
             * On dit ici que la propriété squats est de type
             * CollectionType, car un chat pourra avoir plusieurs
             * squats. On doit ensuite lui dire dans les options
             * le type de collection qu'on a, en l'occurrence, des
             * SquatType (on fait référence à un autre formulaire)
             */
            ->add('squats', CollectionType::class, [
                'entry_type' => SquatType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Kitten::class,
        ]);
    }
}
