<?php

namespace App\Repository;

use App\Entity\Squat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Squat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Squat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Squat[]    findAll()
 * @method Squat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SquatRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Squat::class);
    }

//    /**
//     * @return Squat[] Returns an array of Squat objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Squat
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
